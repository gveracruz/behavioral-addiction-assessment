# Behavioral addiction assessment

This project aim at examining the psychometric properties of different instruments assessing behavioral addiction related to Internet based activities, and discuss its ability to discriminate non-pathological cases from pathological ones.
